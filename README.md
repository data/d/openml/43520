# OpenML dataset: 5-year-BSE-Sensex-Dataset

https://www.openml.org/d/43520

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The BSE SENSEX (also known as the SP Bombay Stock Exchange Sensitive Index or simply the SENSEX) is a free-float market-weighted stock market index of 30 well-established and financially sound companies listed on Bombay Stock Exchange. The 30 constituent companies which are some of the largest and most actively traded stocks, are representative of various industrial sectors of the Indian economy.

Content
Dataset includes few fundamental statistics of SENSEX. Can you come up with a screener to select investment worthy opportunities? Can you find out any trend of last 5 years from 2015 to 2020.
What other fundamental indicators would be look at? Comment and I may add them in future versions.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43520) of an [OpenML dataset](https://www.openml.org/d/43520). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43520/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43520/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43520/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

